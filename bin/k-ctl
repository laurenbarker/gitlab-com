#!/usr/bin/env bash
# vim: ai:ts=2:sw=2:et

##############################
# wrapper script for running
# helm against this chart

set -euf -o pipefail

dir="$(
  cd "$(dirname "${0}")"
  pwd
)"

CHART="${CHART:-gitlab}"

# Source common functions and variable exports
# that are common to all charts

COMMON_SCRIPT_PATH="${COMMON_SCRIPT_PATH:-/k8s-workloads/common.bash}"
AUTO_DEPLOY="${AUTO_DEPLOY:-false}"

if [[ -r "$COMMON_SCRIPT_PATH" ]]; then
  source "$COMMON_SCRIPT_PATH"
else
  # Grab the CI image version from  the .gitlab-ci.yml
  _BBLK="\\033[1;30m"
  _NORM="\\033[0m"
  version=$(sed -E -e "s/[[:space:]]+CI_IMAGE_VERSION:[[:space:]]'(v[0-9\\.]+)'/\\1/" -e "t" -e "d" .gitlab-ci.yml)
  echo -e "${_BBLK}Sourcing version $version of the common shell script"
  echo -e "if you want to use a local version set COMMON_SCRIPT_PATH to the location of common.bash"
  echo -e "or update .gitlab-ci.yml to set a new version${_NORM}"
  echo ""
  source <(curl -s "https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/common/raw/$version/bin/common.bash")
fi

EXPEDITE_DEPLOYMENT=${EXPEDITE_DEPLOYMENT:-false}
NAME="${NAME:-gitlab${STAGE_SUFFIX}}"
NAMESPACE="${NAMESPACE:-gitlab${STAGE_SUFFIX}}"
CHARTS_DIR=$(mktemp -d)
MANIFESTS_DIR="$dir/../manifests"
HELM_DIFF_TMP_FNAME="/tmp/helm-diff"

is_semver() {
  if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+ ]]; then
    return 0
  else
    return 1
  fi
}

environment=${ENV}${STAGE_SUFFIX}
auto_deploy_changes_fname="${dir}/../auto-deploy-image-check/${environment}.json"
helmfile_common_options=(
  "--log-level" "${LOG_LEVEL:-info}"
  "--environment" "${environment}"
)

helmfile_cmd_options=(
  "--concurrency" "1"
  "--skip-deps"
  "--suppress-secrets"
)

if [[ $AUTO_DEPLOY == "true" ]]; then
  helmfile_common_options+=("-l" "autodeploy=true")
fi

json_cmp() {
  result=$(jq --argfile a "$1" --argfile b "$2" -n '($a | (.. | arrays) |= sort | del(.[] | select(.name | test("gitlab-.*registry-migrations-.*")))) as $a | ($b | (.. | arrays) |= sort) as $b | $a == $b')

  if [[ $result == "true" ]]; then
    return 0
  else
    return 1
  fi
}

helm_diff() {
  local diff_extra_args=$1
  helmfile "${helmfile_common_options[@]}" diff "${helmfile_cmd_options[@]}" --context=10 --args "$diff_extra_args"
}

auto_deploy_check() {
  # Checks to ensure that the changes exactly match
  # what we expect for an image-only update
  if [[ ${POST_DEPLOYMENT_PATCH:-} == "true" ]]; then
    debug "Image checks are skipped for post-deployment checks"
    return
  fi

  local diff_file=$1

  if [[ $(jq '. | length' "$diff_file") -eq 0 ]]; then
    debug "✅ There are no changes detected" "$_BGRN"
    return
  fi

  if json_cmp "$diff_file" "$auto_deploy_changes_fname"; then
    debug "✅ The number of changes are correct for an auto-deploy image update" "$_BGRN"
    return
  fi

  debug "❌ Unexpected number of changes for an image-only update" "$_BRED"
  debug ""
  debug "Expected:" "$_CYN"
  debug "-------"
  cat "$auto_deploy_changes_fname" >&2
  debug ""
  debug "Full diff:" "$_CYN"
  debug "-------"
  helm_diff ""
  debug ""
  debug "Check https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/pipelines to see if there are any outstanding configuration changes, to override this failure set IMAGE_CHECK_OVERRIDE=true as a CI variable" "$_CYN"

  if [[ ${IMAGE_CHECK_OVERRIDE:-} == "true" ]]; then
    debug "WARNING: Bypassing auto-deploy image check because IMAGE_CHECK_OVERRIDE is set to true as a CI variable" "$_RED"
    return
  fi

  exit 1
}

case "$ACTION" in
  install | upgrade)
    overview

    if [[ ${dry_run:-} == "true" ]]; then
      debug "-- Helm Diff --" "${_CYN}"
      # Send a diff notification, allowed to
      # fail so we don't block the pipeline
      # Skip the notifications for auto-deploy triggered pipelines since they are not
      # done in the context of an MR.
      if [[ $AUTO_DEPLOY == "false" ]]; then
        helm_diff "" | tee "$HELM_DIFF_TMP_FNAME"
        if [[ -f /k8s-workloads/notify-mr ]] && ! "${EXPEDITE_DEPLOYMENT}"; then
          /k8s-workloads/notify-mr -d "$HELM_DIFF_TMP_FNAME" -e "$environment" || echo "WARNING: notify-mr diff notification failed"
        fi
      else
        helm_diff "--output=json" | tee "$HELM_DIFF_TMP_FNAME"
        # helm diff has an extra line that starts with "Comparing release"
        # that needs to be removed.
        sed -i -e '/^Comparing release/d' "$HELM_DIFF_TMP_FNAME"
        auto_deploy_check "$HELM_DIFF_TMP_FNAME"
      fi
    else
      ./bin/watch-gitlab-deployments.sh "${NAMESPACE}" &

      helmfile "${helmfile_common_options[@]}" apply "${helmfile_cmd_options[@]}"
    fi
    ;;

  list)
    # `helmfile` outputs less information for this command
    # since `helm` does better, let's leave that in place
    debug "-- Helm List --" "${_CYN}"
    helm list
    ;;

  remove)
    overview
    warn_removal

    # Remove secrets by removing gitlab-secrets release via helmfile
    if [[ ${dry_run:-} != "true" ]]; then
      helmfile "${helmfile_common_options[@]}" destroy
    else
      debug "Would run: helmfile ${helmfile_common_options[*]} destroy"
    fi
    ;;

  template)
    overview

    helmfile "${helmfile_common_options[@]}" template --output-dir "$MANIFESTS_DIR" --concurrency 1
    ;;
esac

set +x
rm -rf "${CHARTS_DIR:?}"
