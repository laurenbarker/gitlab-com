---

include:
  - '/ci/deploy.yml'
  - '/ci/except-com.yml'
  - '/ci/except-ops.yml'
  - '/ci/cluster-init-before-script.yml'
  - '/ci/shellcheck.yml'
  - '/ci/version-checks.yml'
  - '/ci/check-vendored-charts.yml'
  # Dependency scanning
  # https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
  - template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  CI_IMAGE_VERSION: 'v5.15.0'
  AUTO_DEPLOY: 'false'

#############################################################

dependency_scanning:
  stage: check
bundler-audit-dependency_scanning:
  stage: check
retire-js-dependency_scanning:
  stage: check

.pre-base:
  variables:
    PROJECT: gitlab-pre
    REGION: us-east1
  environment:
    name: pre
    url: https://pre.gitlab.com

.pre:
  extends:
    - .pre-base
  variables:
    CLUSTER: pre-gitlab-gke
  environment:
    name: pre

.gstg-base:
  variables:
    PROJECT: gitlab-staging-1
  environment:
    url: https://staging.gitlab.com

.gstg:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-gitlab-gke
    REGION: us-east1
  environment:
    name: gstg
  resource_group: gstg

.gstg-us-east1-b:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-us-east1-b
    REGION: us-east1-b
  environment:
    name: gstg-us-east1-b
  resource_group: gstg-us-east1-b

.gstg-us-east1-c:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-us-east1-c
    REGION: us-east1-c
  environment:
    name: gstg-us-east1-c
  resource_group: gstg-us-east1-c

.gstg-us-east1-d:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-us-east1-d
    REGION: us-east1-d
  environment:
    name: gstg-us-east1-d
  resource_group: gstg-us-east1-d

.gprd-base:
  variables:
    PROJECT: gitlab-production
  environment:
    url: https://gitlab.com

.gprd:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-gitlab-gke
    REGION: us-east1
  environment:
    name: gprd
  resource_group: gprd

.gprd-us-east1-b:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-us-east1-b
    REGION: us-east1-b
  environment:
    name: gprd-us-east1-b
  resource_group: gprd-us-east1-b

.gprd-us-east1-c:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-us-east1-c
    REGION: us-east1-c
  environment:
    name: gprd-us-east1-c
  resource_group: gprd-us-east1-c

.gprd-us-east1-d:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-us-east1-d
    REGION: us-east1-d
  environment:
    name: gprd-us-east1-d
  resource_group: gprd-us-east1-d

.only-auto-deploy-false:
  only:
    variables:
      - $AUTO_DEPLOY == "false" && $CI_PIPELINE_SOURCE != "schedule"

.only-auto-deploy-false-and-config-changes:
  only:
    variables:
      - $AUTO_DEPLOY == "false" && $CI_PIPELINE_SOURCE != "schedule"
    changes:
      - 'charts/gitlab/**/*'
      - 'bases/**/*'
      - 'bin/**/*'
      - 'releases/**/*'
      - '*.yaml'
      - .gitlab-ci.yml
      - CHEF_CONFIG_UPDATE

.trigger-qa-smoke:
  extends:
    - .except-com
    # Skip QA smoke tests for auto-deploy because
    # QA is run in the context of the deployer
    # pipeline
    - .only-auto-deploy-false-and-config-changes
  image: alpine:3.12.0
  # This script can be replaced with the `trigger:` keyword
  # when the product supports retries for triggers
  # https://gitlab.com/gitlab-org/gitlab/-/issues/32559
  except:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"
      - $CI_COMMIT_REF_NAME != 'master'
    refs:
      - tags
  script:
    - |
      if [[ $SKIP_QA == "true" ]]; then
        echo "Skipping QA because SKIP_QA is set to true"
        exit 0
      fi
    - apk add curl jq
    # project=full/path/to/project
    - |
      echo "Sending trigger to $project"
      # URL encode the project
      project=$(echo -n "$project" | jq -sRr @uri)
      trigger_url="$CI_API_V4_URL/projects/$project/trigger/pipeline"
      resp=$(curl -s --request POST --form "variables[SMOKE_ONLY]=true" --form "token=$CI_JOB_TOKEN" --form ref=master $trigger_url)
      id=$(echo "$resp" | jq -r ".id")
      web_url=$(echo "$resp" | jq -r ".web_url")
    - |
      echo "Waiting for pipeline $web_url ..."
      status_url="$CI_API_V4_URL/projects/$project/pipelines/$id"
      for retry in $(seq 1 120); do
        resp=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_OPS_API_TOKEN" "$status_url")
        status=$(echo "$resp" | jq -r '.status')
        echo "Got pipeline status $status, retry $retry/10"
        [[ $status == "success" || $status == "failed" || $status == "canceled" ]] && break
        sleep 30
      done

      if [[ $status != "success" ]]; then
        echo "$web_url has status $status, failing"
        exit 1
      fi

stages:
  - check
  - dryrun-check
  - dryrun
  - non-prod-cny:deploy
  - non-prod:deploy
  - non-prod:QA
  - gprd-cny:deploy
  - gprd:deploy:alpha
  - gprd:deploy:beta
  - cleanup
  - scheduled

#########################################
# Attempts to find an MR on .com and add
# a link to the pipeline that runs on ops

notify_com_mr:
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/notify-mr:${CI_IMAGE_VERSION}"
  stage: check
  script:
    - notify-mr -s
  # It is possible that this job will fail if a branch is created
  # without a corresponding MR, for this reason we allow it to
  # fail
  allow_failure: true
  dependencies: []
  except:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"
      - $EXPEDITE_DEPLOYMENT
  only:
    variables:
      - $AUTO_DEPLOY == "false" && $CI_PIPELINE_SOURCE != "schedule"

###############################################################################
### Configuration Jobs ###
#
# Jobs here are specific to changes to configurations.  These will construct a
# pipeline that targets all environments running QA inbetween non-prod and
# prod environments. The pipeline is triggered by changes to this repository.
#
#  Reference: `.only-auto-deploy-false-and-config-changes`
#
# Changes to infrastructure only occur when we create a pipeline on changes to
# the `master` branch

### PreProd

pre:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .pre
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

pre:upgrade:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .pre
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - bin/k-ctl upgrade
  resource_group: pre
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

### Staging Canary

gstg-cny:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gstg
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D -s cny upgrade
  resource_group: gstg-cny

gstg-cny:upgrade:
  stage: non-prod-cny:deploy
  extends:
    - .deploy
    - .gstg
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME -s cny
    - sendEvent "Starting k8s configuration for $CLUSTER" "gstg" "configuration" "cny"
    - bin/k-ctl -s cny upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gstg" "configuration" "cny"
  resource_group: gstg-cny
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

### Staging

gstg:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gstg
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gstg-us-east1-b:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gstg-us-east1-b
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gstg-us-east1-c:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gstg-us-east1-c
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gstg-us-east1-d:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gstg-us-east1-d
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gstg:upgrade:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gstg" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gstg" "configuration"
  resource_group: gstg
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

gstg-us-east1-b:upgrade:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg-us-east1-b
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gstg" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gstg" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

gstg-us-east1-c:upgrade:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg-us-east1-c
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gstg" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gstg" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

gstg-us-east1-d:upgrade:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg-us-east1-d
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gstg" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gstg" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

### Production Canary

gprd-cny:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gprd
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D -s cny upgrade
  resource_group: gprd-cny

gprd-cny:upgrade:
  stage: gprd-cny:deploy
  extends:
    - .deploy
    - .gprd
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME -s cny
    - sendEvent "Starting k8s configuration for $CLUSTER" "gprd" "configuration" "cny"
    - bin/k-ctl -s cny upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gprd" "configuration" "cny"
  resource_group: gprd-cny
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

### Production

gprd:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gprd
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gprd-us-east1-b:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gprd-us-east1-b
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gprd-us-east1-c:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gprd-us-east1-c
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gprd-us-east1-d:dryrun:
  stage: dryrun
  extends:
    - .deploy
    - .gprd-us-east1-d
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/k-ctl -D upgrade

gprd:upgrade:
  stage: gprd:deploy:alpha
  extends:
    - .deploy
    - .gprd
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gprd" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gprd" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

gprd-us-east1-b:upgrade:
  stage: gprd:deploy:alpha
  extends:
    - .deploy
    - .gprd-us-east1-b
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gprd" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gprd" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

gprd-us-east1-c:upgrade:
  stage: gprd:deploy:beta
  extends:
    - .deploy
    - .gprd-us-east1-c
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gprd" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gprd" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

gprd-us-east1-d:upgrade:
  stage: gprd:deploy:beta
  extends:
    - .deploy
    - .gprd-us-east1-d
    - .only-auto-deploy-false-and-config-changes
  script:
    - bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
    - sendEvent "Starting k8s configuration for $CLUSTER" "gprd" "configuration"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s configuration for $CLUSTER" "gprd" "configuration"
  only:
    refs:
      - master
  variables:
    DRY_RUN: "false"

### QA jobs

pre:qa:
  extends:
    - .trigger-qa-smoke
  stage: non-prod:QA
  # Allowing failure for preprod QA since it is not something
  # we want to block on for prod deployments.
  allow_failure: true
  variables:
    project: gitlab-org/quality/preprod
  except:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"
      - $CI_COMMIT_REF_NAME != 'master'
      - $EXPEDITE_DEPLOYMENT
  only:
    changes:
      - 'charts/gitlab/**/*'
      - '.gitlab-ci.yml'
      - '*.yaml'
      - 'releases/gitlab-secrets/*'
      - 'releases/gitlab/helmfile.yaml'
      - 'releases/gitlab/values/jq/*'
      - 'releases/gitlab/values/values*'
      - 'releases/gitlab/values/pre*'

gstg:qa:
  extends:
    - .trigger-qa-smoke
  stage: non-prod:QA
  variables:
    project: gitlab-org/quality/staging
  except:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"
      - $CI_COMMIT_REF_NAME != 'master'
      - $EXPEDITE_DEPLOYMENT
  only:
    changes:
      - 'charts/gitlab/**/*'
      - '.gitlab-ci.yml'
      - '*.yaml'
      - 'releases/gitlab-secrets/*'
      - 'releases/gitlab/helmfile.yaml'
      - 'releases/gitlab/values/jq/*'
      - 'releases/gitlab/values/values*'
      - 'releases/gitlab/values/gstg*'

###############################################################################
### Deploy Jobs ###
#
# Jobs here are specific to deployments.  We only support targeting one
# environment at a time.  These are triggered by external tooling using these
# specific variables:
#
#   * ENVIRONMENT - configures CI to create a pipeline targeting one environment
#   * DRY_RUN - if true, does not make changes to infrastructure
#   * AUTO_DEPLOY - set via external tooling, only runs deploy related jobs

### PreProd

pre:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .pre
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "pre" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

pre:auto-deploy:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .pre
  script:
    - bin/k-ctl upgrade
  only:
    variables:
      - $ENVIRONMENT == "pre" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

### Staging Canary

gstg-cny:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gstg
  script:
    - bin/k-ctl -D -s cny upgrade
  only:
    variables:
      - $ENVIRONMENT == "gstg-cny" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"
  resource_group: gstg-cny

gstg-cny:auto-deploy:
  stage: non-prod-cny:deploy
  extends:
    - .deploy
    - .gstg
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gstg" "deployment" "cny"
    - bin/k-ctl -s cny upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gstg" "deployment" "cny"
  resource_group: gstg-cny
  only:
    variables:
      - $ENVIRONMENT == "gstg-cny" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

### Staging

gstg:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gstg
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg-us-east1-b:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gstg-us-east1-b
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg-us-east1-c:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gstg-us-east1-c
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg-us-east1-d:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gstg-us-east1-d
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg:auto-deploy:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gstg" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gstg" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg-us-east1-b:auto-deploy:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg-us-east1-b
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gstg" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gstg" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg-us-east1-c:auto-deploy:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg-us-east1-c
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gstg" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gstg" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gstg-us-east1-d:auto-deploy:
  stage: non-prod:deploy
  extends:
    - .deploy
    - .gstg-us-east1-d
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gstg" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gstg" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gstg" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

### Production Canary

gprd-cny:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gprd
  script:
    - bin/k-ctl -D -s cny upgrade
  only:
    variables:
      - $ENVIRONMENT == "gprd-cny" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"
  resource_group: gprd-cny

gprd-cny:auto-deploy:
  stage: gprd-cny:deploy
  extends:
    - .deploy
    - .gprd
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gprd" "deployment" "cny"
    - bin/k-ctl -s cny upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gprd" "deployment" "cny"
  resource_group: gprd-cny
  only:
    variables:
      - $ENVIRONMENT == "gprd-cny" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

### Production

gprd:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gprd
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd-us-east1-b:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gprd-us-east1-b
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd-us-east1-c:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gprd-us-east1-c
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd-us-east1-d:dryrun:auto-deploy:
  stage: dryrun
  extends:
    - .deploy
    - .gprd-us-east1-d
  script:
    - bin/k-ctl -D upgrade
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd:auto-deploy:
  stage: gprd:deploy:alpha
  extends:
    - .deploy
    - .gprd
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gprd" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gprd" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd-us-east1-b:auto-deploy:
  stage: gprd:deploy:alpha
  extends:
    - .deploy
    - .gprd-us-east1-b
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gprd" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gprd" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd-us-east1-c:auto-deploy:
  stage: gprd:deploy:beta
  extends:
    - .deploy
    - .gprd-us-east1-c
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gprd" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gprd" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

gprd-us-east1-d:auto-deploy:
  stage: gprd:deploy:beta
  extends:
    - .deploy
    - .gprd-us-east1-d
  script:
    - sendEvent "Starting k8s deployment for $CLUSTER" "gprd" "deployment"
    - bin/k-ctl upgrade
    - sendEvent "Finished k8s deployment for $CLUSTER" "gprd" "deployment"
  only:
    variables:
      - $ENVIRONMENT == "gprd" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"

###############################################################################
# Clean up
#   * These jobs run cleanup related tasks
###############################################################################

remove-expedite-variable:
  stage: cleanup
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}"
  script:
    - "curl --fail --header \"PRIVATE-TOKEN: ${OPS_API_TOKEN}\" -X DELETE \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables/EXPEDITE_DEPLOYMENT\""
  rules:
    - if: '$CI_API_V4_URL == "https://gitlab.com/api/v4"'
      when: never
    - if: '$AUTO_DEPLOY == "true"'
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: '($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH) && $EXPEDITE_DEPLOYMENT'

###############################################################################
# scheduled
#   * These jobs run on a schedule for maintenance tasks
###############################################################################

open-chart-bump-mr-pre:
  stage: scheduled
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}"
  script:
    - chmod 400 "$SSH_PRIVATE_KEY"
    - export GIT_SSH_COMMAND="ssh -i $SSH_PRIVATE_KEY -o IdentitiesOnly=yes -o GlobalKnownHostsFile=$SSH_KNOWN_HOSTS"
    - git config --global user.email "ops@ops.gitlab.net"
    - git config --global user.name "ops-gitlab-net"
    - git remote set-url origin https://ops-gitlab-net:${GITLAB_API_TOKEN}@gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com.git
    - echo $GITLAB_API_TOKEN | glab auth login --hostname gitlab.com --stdin
    - glab config set git_protocol https
    - glab auth status
    - ./bin/autobump-gitlab-chart.sh pre
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - when: never

open-chart-bump-mr-gstg:
  stage: scheduled
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}"
  script:
    - chmod 400 "$SSH_PRIVATE_KEY"
    - export GIT_SSH_COMMAND="ssh -i $SSH_PRIVATE_KEY -o IdentitiesOnly=yes -o GlobalKnownHostsFile=$SSH_KNOWN_HOSTS"
    - git config --global user.email "ops@ops.gitlab.net"
    - git config --global user.name "ops-gitlab-net"
    - git remote set-url origin https://ops-gitlab-net:${GITLAB_API_TOKEN}@gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com.git
    - echo $GITLAB_API_TOKEN | glab auth login --hostname gitlab.com --stdin
    - glab config set git_protocol https
    - glab auth status
    - ./bin/autobump-gitlab-chart.sh gstg
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - when: never

open-chart-bump-mr-gprd:
  stage: scheduled
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}"
  script:
    - chmod 400 "$SSH_PRIVATE_KEY"
    - export GIT_SSH_COMMAND="ssh -i $SSH_PRIVATE_KEY -o IdentitiesOnly=yes -o GlobalKnownHostsFile=$SSH_KNOWN_HOSTS"
    - git config --global user.email "ops@ops.gitlab.net"
    - git config --global user.name "ops-gitlab-net"
    - git remote set-url origin https://ops-gitlab-net:${GITLAB_API_TOKEN}@gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com.git
    - echo $GITLAB_API_TOKEN | glab auth login --hostname gitlab.com --stdin
    - glab config set git_protocol https
    - glab auth status
    - ./bin/autobump-gitlab-chart.sh gprd
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - when: never
