---

# Please note that `gprd.yaml.gotmpl` is included first for the `gprd-cny` environment,
# this file is for `cny` specific overrides

nginx-ingress:
  controller:
    autoscaling:
      minReplicas: 3
    labels: &nginx_labels
      shard: default
      stage: cny
    podLabels:
      <<: *nginx_labels
    service:
      labels:
        <<: *nginx_labels

gitlab:
  gitlab-pages:
    gitlabCache:
      expiry: 30m
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-pages@{{ .Values.google_project }}.iam.gserviceaccount.com
  gitlab-shell:
    minReplicas: 8
    nodeSelector:
      cloud.google.com/gke-nodepool: shell-cny-1
    extraEnv:
      GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-shell
    metrics:
      enabled: true
    sshDaemon: gitlab-sshd
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    service:
      # gcloud compute address ssh-gke-gprd-cny
      loadBalancerIP: 10.216.8.61
  mailroom:
    enabled: false
    image:
      repository: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-mailroom
      # Pin the tag to avoid following the chart default
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/201
      tag: 0.0.19
      pullSecrets:
        - name: dev-registry-access-v1

  sidekiq:
    enabled: false
  webservice:
    common:
      labels:
        shard: default
        stage: cny
        tier: sv
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"cny\"}"
        hpa:
          # keeping minReplicas high enough to not suffer from node scaling
          # during deployments (https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1592)
          minReplicas: 16
        service:
          # gcloud compute address api-gke-gprd-cny
          loadBalancerIP: 10.216.8.13
      git:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-git
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
        nodeSelector:
          cloud.google.com/gke-nodepool: git-https-3
        service:
          # gcloud compute address git-https-gke-gprd-cny
          loadBalancerIP: 10.216.8.10
      web:
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"web\", \"stage\": \"cny\"}"
          CANARY: "true"
        hpa:
          minReplicas: 16
          maxReplicas: 50
          targetAverageValue: 3600m
        workerProcesses: 6
        resources:
          limits:
            memory: 12G
          requests:
            cpu: 4500m
            memory: 7G
        service:
          # gcloud compute address web-gke-gprd-cny
          loadBalancerIP: 10.216.8.87
      websockets:
        hpa:
          minReplicas: 1
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-websockets
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"websockets\", \"stage\": \"cny\"}"
          GODEBUG: madvdontneed=1
        service:
          # gcloud compute address websockets-gke-gprd-cny
          loadBalancerIP: 10.216.8.15
    minReplicas: 5
    extraEnv:
      GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
      DISABLE_PUMA_NAKAYOSHI_FORK: "true"
      GITLAB_LOG_DEPRECATIONS: "true"
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-webservice@{{ .Values.google_project }}.iam.gserviceaccount.com

global:
  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-gprd-cny
      - 10.216.8.25
    externalHttps:
      # gcloud compute address pages-gke-gprd-cny
      - 10.216.8.25

registry:
  hpa:
    # min is set higher for https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2380
    minReplicas: 20
  service:
    # gcloud compute address registry-gke-gprd-cny
    loadBalancerIP: 10.216.8.18
  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: gitlab-cny-registry@{{ .Values.google_project }}.iam.gserviceaccount.com
  storage:
    secret: registry-storage-v4
  # When enabled, the upload purger will attempt to make deletes against the
  # common GCS bucket. It is not suitable to be enabled in production until the
  # following issues are resolved:
  # https://gitlab.com/gitlab-org/container-registry/-/issues/216
  # https://gitlab.com/gitlab-org/container-registry/-/issues/217
  maintenance:
    uploadpurging:
      enabled: false
  gc:
    disabled: false
