---

# Force PreProd config for https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1163
nginx-ingress:
  controller:
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                  - key: app.kubernetes.io/name
                    operator: In
                    values:
                      - ingress-nginx
                  - key: app.kubernetes.io/instance
                    operator: In
                    values:
                      - ingress-nginx
                  - key: app.kubernetes.io/component
                    operator: In
                    values:
                      - controller
              topologyKey: kubernetes.io/hostname

  nodeSelector:
    cloud.google.com/gke-nodepool: default-2

registry:
  middleware:
    storage:
      - name: googlecdn
        options:
          baseurl: cdn.registry.pre.gitlab-static.net
          privatekeySecret:
            secret: registry-cdn-private-key-v1
            key: private-key
          keyname: pre-registry-cdn
          ipfilteredby: gcp
  database:
    enabled: true
    host: 10.33.1.2
    user: registry
    name: registry_production
    password:
      secret: registry-postgresql-password-v1
    pool:
      maxopen: 5
      maxidle: 5
      maxlifetime: 5m
  migration:
    enabled: true
    disablemirrorfs: true
    rootdirectory: gitlab
    importtimeout: 30m
    preimporttimeout: 48h
    importnotification:
      enabled: true
  nodeSelector:
    cloud.google.com/gke-nodepool: default-2
  gc:
    disabled: false
    maxbackoff: 30m
    reviewafter: 5m
    noidlebackoff: true
  service:
    # gcloud compute address registry-gke-pre
    loadBalancerIP: 10.232.20.237
  storage:
    secret: registry-storage-v4
  maintenance:
    uploadpurging:
      enabled: false
  validation:
    disabled: false
    manifests:
      referencelimit: 200
      payloadsizelimit: 256000
      urls:
        # this is needed to keep backwards compatibility when `validation.disabled: false`, as by default an empty `allow` means no URLs are allowed
        allow:
          - .*

gitlab:
  gitlab-pages:
    hpa:
      minReplicas: 1
      targetAverageValue: 400m
    nodeSelector:
      cloud.google.com/gke-nodepool: default-2
    resources:
      requests:
        cpu: 500m
        memory: 70M
      limits:
        memory: 1G
    extraEnv:
      FF_HANDLE_CACHE_HEADERS: "true"
      FF_ENFORCE_IP_RATE_LIMITS: "true"
      FF_ENFORCE_DOMAIN_RATE_LIMITS: "true"
    rateLimitSourceIP: 20
    rateLimitSourceIPBurst: 1000
    rateLimitDomain: 100
    rateLimitDomainBurst: 500
    rateLimitTLSDomain: 30
    rateLimitTLSDomainBurst: 100
    zipCache:
      expiration: 300s
  webservice:
    hpa:
      minReplicas: 2
      maxReplicas: 5
    # blackoutSeconds, minReplicas, initialDelaySeconds, terminationGracePeriodSeconds
    # set temporarily for
    # testing https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1509
    shutdown:
      blackoutSeconds: 0
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"main\"}"
        ingress:
          path: '/api'
        nodeSelector:
          cloud.google.com/gke-nodepool: default-2
        service:
          # gcloud compute address api-gke-pre
          loadBalancerIP: 10.232.20.95
      git:
        nodeSelector:
          cloud.google.com/gke-nodepool: default-2
        service:
          # gcloud compute address git-https-gke-pre
          loadBalancerIP: 10.232.20.118
        shutdown:
          blackoutSeconds: 0
      web:
        nodeSelector:
          cloud.google.com/gke-nodepool: default-2
        service:
          # gcloud compute address web-gke-pre
          loadBalancerIP: 10.232.20.112
      websockets:
        deployment:
          readinessProbe:
            initialDelaySeconds: 0
          terminationGracePeriodSeconds: 30
        nodeSelector:
          cloud.google.com/gke-nodepool: default-2
        service:
          # gcloud compute address websockets-gke-pre
          loadBalancerIP: 10.232.20.115
        shutdown:
          blackoutSeconds: 0
    rack_attack:
      git_basic_auth:
        enabled: false
    workerProcesses: 2
    workhorse:
      resources:
        limits:
          memory: 1G
        requests:
          cpu: 100m
          memory: 50M
    resources:
      limits:
        memory: 4.0G
      requests:
        cpu: 1
        memory: 1.25G
    extraEnv:
      DISABLE_PUMA_NAKAYOSHI_FORK: "true"
      # https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5950
      GITLAB_ENABLE_QUERY_ANALYZERS: "true"
      ENABLE_CROSS_DATABASE_MODIFICATION_DETECTION: "true"
  gitlab-shell:
    extraEnv:
      GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-shell
    metrics:
      enabled: true
    minReplicas: 2
    maxReplicas: 5
    nodeSelector:
      cloud.google.com/gke-nodepool: default-2
    service:
      # gcloud compute address ssh-gke-pre
      loadBalancerIP: 10.232.20.28
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    sshDaemon: gitlab-sshd
  mailroom:
    image:
      repository: dev.gitlab.org:5005/gitlab/charts/components/images/gitlab-mailroom
      # Pin the tag to avoid following the chart default
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/201
      tag: 0.0.19
      pullSecrets:
        - name: dev-registry-access-v1
    nodeSelector:
      cloud.google.com/gke-nodepool: default-2
    workhorse:
      serviceName: webservice-api
  sidekiq:
    pods:
      - name: catchall
        common:
          labels:
            shard: catchall
        nodeSelector:
          cloud.google.com/gke-nodepool: default-2
        concurrency: 25
        minReplicas: 1
        maxReplicas: 5
    extraEnv:
      # https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5950
      GITLAB_ENABLE_QUERY_ANALYZERS: "true"
      ENABLE_CROSS_DATABASE_MODIFICATION_DETECTION: "true"
    psql:
      host: 10.33.1.14
  kas:
    hpa:
      minReplicas: 1
      maxReplicas: 5
    nodeSelector:
      cloud.google.com/gke-nodepool: default-2
    service:
      # gcloud compute address kas-internal-gke-pre
      loadBalancerIP: 10.232.20.117
global:
  appConfig:
    artifacts:
      bucket: gitlab-pre-artifacts
    contentSecurityPolicy:
      enabled: true
      report_only: false
      directives:
        connect_src: "'self' https://pre.gitlab.com https://pre.gitlab-static.net wss://pre.gitlab.com https://sentry.gitlab.net https://customers.gitlab.com https://snowplow.trx.gitlab.net https://sourcegraph.com https://ec2.ap-east-1.amazonaws.com https://ec2.ap-northeast-1.amazonaws.com https://ec2.ap-northeast-2.amazonaws.com https://ec2.ap-northeast-3.amazonaws.com https://ec2.ap-south-1.amazonaws.com https://ec2.ap-southeast-1.amazonaws.com https://ec2.ap-southeast-2.amazonaws.com https://ec2.ca-central-1.amazonaws.com https://ec2.eu-central-1.amazonaws.com https://ec2.eu-north-1.amazonaws.com https://ec2.eu-west-1.amazonaws.com https://ec2.eu-west-2.amazonaws.com https://ec2.eu-west-3.amazonaws.com https://ec2.me-south-1.amazonaws.com https://ec2.sa-east-1.amazonaws.com https://ec2.us-east-1.amazonaws.com https://ec2.us-east-2.amazonaws.com https://ec2.us-west-1.amazonaws.com https://ec2.us-west-2.amazonaws.com https://ec2.af-south-1.amazonaws.com https://iam.amazonaws.com"
        default_src: "'self' https://pre.gitlab-static.net"
        frame_ancestors: "'self'"
        frame_src: "'self' https://pre.gitlab-static.net https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://content.googleapis.com https://content-cloudresourcemanager.googleapis.com https://content-compute.googleapis.com https://content-cloudbilling.googleapis.com https://*.codesandbox.io"
        img_src: "* data: blob:"
        object_src: "'none'"
        report_uri: "https://sentry.gitlab.net/api/22/security/?sentry_key=e9401448c5c04c39823793199b8f7c49"
        script_src: "'self' 'unsafe-inline' 'unsafe-eval' https://pre.gitlab-static.net https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https://www.recaptcha.net/ https://apis.google.com"
        style_src: "'self' 'unsafe-inline' https://pre.gitlab-static.net"
        worker_src: "https://pre.gitlab-static.net https://pre.gitlab.com blob: data:"
    dependencyProxy:
      enabled: true
      bucket: gitlab-pre-dependency-proxy
    externalDiffs:
      bucket: gitlab-pre-external-diffs
    incomingEmail:
      address: "incoming-pre+%{key}@incoming.gitlab.com"
      user: incoming-pre@incoming.gitlab.com
      deliveryMethod: webhook
      authToken:
        secret: gitlab-mailroom-imap-v2
        key: incoming_email_auth_token
    lfs:
      bucket: gitlab-pre-lfs-objects
    omniauth:
      providers:
        - secret: gitlab-google-oauth2-v1
    packages:
      bucket: gitlab-pre-package-repo
    sentry:
      dsn: https://e9401448c5c04c39823793199b8f7c49@sentry.gitlab.net/22
    terraformState:
      bucket: gitlab-pre-terraform-state
    uploads:
      bucket: gitlab-pre-uploads

  email:
    from: notify@mg.pre.gitlab.com
    reply_to: noreply@pre.gitlab.com

  gitaly:
    external:
      - hostname: gitaly-01-sv-pre.c.gitlab-pre.internal
        name: default
        port: "9999"
        tlsEnabled: false
      - hostname: praefect-01-stor-pre.c.gitlab-pre.internal
        name: praefect
        port: "2305"
        tlsEnabled: false
      - hostname: gitaly-03-sv-pre.c.gitlab-pre.internal
        name: gitaly-03
        port: "9999"
        tlsEnabled: false

  hosts:
    # gcloud compute address nginx-gke-pre
    externalIP: 10.232.20.81
    gitlab:
      name: pre.gitlab.com
    kas:
      name: kas.pre.gitlab.com
    registry:
      name: registry.pre.gitlab.com
  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-pre
      - 10.232.20.119
    externalHttps:
      # gcloud compute address pages-gke-pre
      - 10.232.20.119
    host: pre.gitlab.io
  psql:
    host: 10.33.1.14
  redis:
    host: 10.232.7.3
    password:
      key: secret
      secret: gitlab-redis-credential-v1
    port: "6379"
    rateLimiting:
      host: pre-redis-ratelimiting
      password:
        enabled: true
        key: secret
        secret: gitlab-redis-credential-v1
      sentinels:
      - host: redis-ratelimiting-01-db-pre.c.gitlab-pre.internal
        port: 26379
      - host: redis-ratelimiting-02-db-pre.c.gitlab-pre.internal
        port: 26379
      - host: redis-ratelimiting-03-db-pre.c.gitlab-pre.internal
        port: 26379
  smtp:
    domain: mg.pre.gitlab.com
    user_name: postmaster@mg.pre.gitlab.com
  shell:
    hostKeys:
      secret: gitlab-hostkeys-v1
