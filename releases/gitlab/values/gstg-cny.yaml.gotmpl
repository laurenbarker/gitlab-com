---

# Please note that `gstg.yaml.gotmpl` is included first for the `gstg-cny` environment,
# this file is for `cny` specific overrides

nginx-ingress:
  controller:
    autoscaling:
      minReplicas: 3
    labels: &nginx_labels
      shard: default
      stage: cny
    podLabels:
      <<: *nginx_labels
    service:
      labels:
        <<: *nginx_labels

global:
  hosts:
    # gcloud compute address nginx-gke-gstg-cny
    externalIP: 10.224.34.202
  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-gstg-cny
      - 10.224.34.56
    externalHttps:
      # gcloud compute address pages-gke-gstg-cny
      - 10.224.34.56

  psql:
    ci:
      host: pgbouncer-ci.int.gstg.gitlab.net

gitlab:
  gitlab-pages:
    nodeSelector:
      cloud.google.com/gke-nodepool: default-2
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-pages@{{ .Values.google_project }}.iam.gserviceaccount.com
  gitlab-shell:
    nodeSelector:
      cloud.google.com/gke-nodepool: shell-1
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    service:
      # gcloud compute address ssh-gke-gstg-cny
      loadBalancerIP: 10.224.34.124
  mailroom:
    enabled: false

  sidekiq:
    enabled: false
  webservice:
    common:
      labels:
        shard: default
        stage: cny
        tier: sv
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"cny\"}"
        nodeSelector:
          cloud.google.com/gke-nodepool: api-1
        service:
          # gcloud compute address api-gke-gstg-cny
          loadBalancerIP: 10.224.34.72
      git:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-git
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
        nodeSelector:
          cloud.google.com/gke-nodepool: git-https-1
        service:
          # gcloud compute address git-https-gke-gstg-cny
          loadBalancerIP: 10.224.34.201
      web:
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"web\", \"stage\": \"cny\"}"
          CANARY: "true"
        nodeSelector:
          cloud.google.com/gke-nodepool: web-1
        service:
          # gcloud compute address web-gke-gstg-cny
          loadBalancerIP: 10.224.34.199
      websockets:
        hpa:
          minReplicas: 1
        nodeSelector:
          cloud.google.com/gke-nodepool: web-1
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-websockets
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"websockets\", \"stage\": \"cny\"}"
          GODEBUG: madvdontneed=1
        service:
          # gcloud compute address websockets-gke-gstg-cny
          loadBalancerIP: 10.224.34.200
    minReplicas: 2
    extraEnv:
      GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
      DISABLE_PUMA_NAKAYOSHI_FORK: "true"
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-webservice@{{ .Values.google_project }}.iam.gserviceaccount.com

registry:
  hpa:
    minReplicas: 2
  nodeSelector:
    cloud.google.com/gke-nodepool: registry-0
  service:
    # gcloud compute address registry-gke-gstg-cny
    loadBalancerIP: 10.224.34.100
  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: gitlab-cny-registry@{{ .Values.google_project }}.iam.gserviceaccount.com
